public class Board {
	public Tile [][] grid;
	private final int size = 3;
	
	// constructor
public Board () {
	
	this.grid = new Tile [size][size];
for ( int i =0; i< this.grid.length; i++) {
		for ( int j =0; j< this.grid[i].length; j++)
		this.grid[i][j] = Tile.Blank;
	}
}

  // toString method that creates the board 
public String toString (){
	String output ="";
	for (int i=0; i< this.grid.length; i++){
		output += i + " ";
		for (int j=0; j< this.grid[i].length; j++){
			output += this.grid[i][j] + "  " ;
		}
		output += "\n";
	}
	return output;
 }
 
 // instance method that places the token 
public boolean placeToken(int row, int col, Tile playerToken) {
 if (this.grid.length <= row) {
		return false;
	} 
	if (col > this.grid[0].length || col > this.grid[1].length || col > this.grid[2].length ) {
		return false;
	}
	
	if (this.grid[row][col].equals(Tile.Blank)) {
		this.grid[row][col] = playerToken;
		return true;
	} else {
		return false;
	}
}
	
	// returns true if its full 
public boolean checkIfFull () {
	for ( int i =0; i< this.grid.length; i++) {
		for ( int j =0; j< this.grid[i].length; j++)
		if (this.grid[i][j].equals(Tile.Blank)) {
			return false;
	}
  }
  return true;
}

// checks if a row has the same tokens
private boolean checkIfWinningHorizontal(Tile playerToken) {
	int count= 0;
    for ( int j =0; j< 3; j++) {
 if (this.grid[j][0].equals(playerToken)&&this.grid[j][1].equals(playerToken)&&this.grid[j][2].equals(playerToken)){
			count ++;
		}
    }
	return count == 1;
}
 
//checks if a column has the same tokens 
private boolean checkIfWinningVertical(Tile playerToken) {
	int count= 0;
    for ( int j =0; j< 3; j++) {
 if (this.grid[0][j].equals(playerToken)&&this.grid[1][j].equals(playerToken)&&this.grid[2][j].equals(playerToken)){
			count ++;
		}
    }
	return count == 1;
}

//checks if a diagonal has the same tokens
private boolean checkIfWinningDiagonal(Tile playerToken) {
	int count =0;
	if (this.grid[0][0].equals(playerToken)
		&& this.grid[1][1].equals(playerToken)
	    && this.grid[2][2].equals(playerToken)) {
			count ++;
		}
	if (this.grid[0][2].equals(playerToken)
		&& this.grid[1][1].equals(playerToken)
	    && this.grid[2][0].equals(playerToken)) {
			count ++;
		}
	return count > 0;
}
		
// if one of the private methods is true than returns true 	
public boolean checkIfWinning(Tile playerToken) {
	return checkIfWinningVertical(playerToken) == true || checkIfWinningHorizontal(playerToken) == true 
	|| checkIfWinningDiagonal(playerToken) == true ;
 }
}
