import java.util.Scanner;
public class TicTacToeApp {
	public static void main (String [] args) {
    Scanner reader = new Scanner(System.in);
	//Welcome message 
	System.out.println("Welcome");
	
	// Board object to refer to the board class
	Board myGameBoard = new Board();
	
	// Variables that are initialized 
	boolean gameOver =false;
	int player = 1;
	Tile inPlayerToken = Tile.X;
	
	
	while (gameOver == false ) {
		// Prints the Board
		System.out.println(myGameBoard);
		
		// Shows who's turn it is 
		if (player == 1){
			inPlayerToken = Tile.X;
			System.out.println("player 1's turn");
		} else {
			inPlayerToken = Tile.O;
			System.out.println("player 2's turn");
		}
		
		// Calls the placeToken method in the Board class 
		// if inputed numbers that represent the row and column is not in the board range than it asks to replace your token 
		int inputRow;
		int inputColumn;
		do { 
		System.out.println("Place your token");
		inputRow = reader.nextInt();
		inputColumn = reader.nextInt();
		} while (myGameBoard.placeToken(inputRow, inputColumn, inPlayerToken) == false);
		
		
// Calls the checkIfWinning method 
// if true than player 1 or 2 wins
		if (myGameBoard.checkIfWinning(inPlayerToken) == true ) {
			System.out.println("Player" + player + "is the winner");
			gameOver = true;
         // Calls check if full method 
		 // if true than its a tie 
		} else if (myGameBoard.checkIfFull() == true ) {
			System.out.println("its a tie");
			gameOver = true;
		// else increment the variable player to 1 to swicth turns 
		} else {
			player ++;
			if (player > 2) {
				player = 1;
			}
		}
	}

	}
}