public enum Tile {
	Blank("_"), 
	X("X"),
	O("O");

//fields
private final String name;


//constructor
private Tile(final String name) {
	this.name = name;
}

// get method
public String getName() {
	return this.name;
	}
// toStringMethod
public String toString() {
        return this.name;
    }
}	